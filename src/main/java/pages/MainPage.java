package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MainPage extends ProjectMethods {

	public MainPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.XPATH, using = "//button[@class = '_2AkmmA _29YdH8']") WebElement elecloseLogin;
	
	
	public FlipkartPage closeLogin() {
		click(elecloseLogin);
		return new FlipkartPage();
	}
	
	
}
