package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MobilesPage extends ProjectMethods {
	
	public MobilesPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//h1[@class = '_2yAnYN']") WebElement eleMobileName;
	@FindBy(how = How.XPATH, using = "//div[text() = 'Newest First']") WebElement eleNewestFirst;
	@FindBy(how = How.XPATH, using = "//div[@class = '_3wU53n']") List<WebElement> eleBrands;
	@FindBy(how = How.XPATH, using = "//div[@class = '_1vC4OE _2rQ-NK']") List<WebElement> elePrice;
	@FindBy(how = How.XPATH, using = "//div[@class='_3wU53n']") WebElement eleFirstProduct;
	@FindBy(how = How.CLASS_NAME, using = "_38sUEc") WebElement eleRatings;
	 
	
	public MobilesPage verifyMobileNameTitle() throws InterruptedException {
		verifyPartialText(eleMobileName, "Mi Mobiles");
		/*Thread.sleep(2000);
        verifyTitle("Mi Mobiles");*/
		return this;
	}
	
	public MobilesPage clickNewestFirst() throws InterruptedException {
		click(eleNewestFirst);
		Thread.sleep(2000);
		return this;
	}
	
	public MobilesPage listProducts() {
		for (WebElement Brands : eleBrands) {
			System.out.println(Brands.getText());
		}
		return this;
	}
	
	public MobilesPage listPrice() {
		for (WebElement Price : elePrice) {
			System.out.println(Price.getText());
		}
		return this;
	}
	
	public MobilesPage clickFirstProduct() {
		String text = getText(eleFirstProduct);
		click(eleFirstProduct);
		switchToWindow(1);
		String title = driver.getTitle();
		if(title.contains(text)) {
			System.out.println("Title " + text + " is correct");
		}
		else {
			System.out.println("Title is not correct");
		}
		return this;
	}
	public MobilesPage getRatingsAndReviews() {
		System.out.println(getText(eleRatings));
		return this;
	}
	
}
