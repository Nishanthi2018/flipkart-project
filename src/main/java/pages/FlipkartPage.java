package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FlipkartPage extends ProjectMethods {
	
	public FlipkartPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//span[text() = 'Electronics']") WebElement eleElectronics;
	@FindBy(how = How.LINK_TEXT, using = "Mi") WebElement eleMobile;
	//@FindBy(how = How.XPATH, using = "//h1[@class = '_2yAnYN']") WebElement 
	
	public MobilesPage clickElectronics() throws InterruptedException {
		mouseOver(eleElectronics, eleMobile);
		click(eleMobile);
		Thread.sleep(1000);
		return new MobilesPage();
	}
	
	
}
