package testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class FlipKart {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.flipkart.com/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElementByXPath("//button[@class = '_2AkmmA _29YdH8']").click();
		WebElement element = driver.findElementByXPath("//span[text() = 'Electronics']");
		Actions action = new Actions(driver);
		action.moveToElement(element).perform();
		driver.findElementByLinkText("Mi").click();
		WebElement eleText = driver.findElementByXPath("//h1[@class = '_2yAnYN']");
		String text = eleText.getText();
		if(text.contains("Mi Mobiles")) {
			System.out.println("Page Title "+text+" is populated correctly");
			}
		else {
			System.out.println("Title is wrong");
		}
		driver.findElementByXPath("//div[text() = 'Newest First']").click();
		Thread.sleep(3000);
		List<WebElement> brands = driver.findElementsByXPath("//div[@class = '_3wU53n']");
		for (WebElement eachBrand : brands) {
			System.out.println(eachBrand.getText());
		}
		List<WebElement> price = driver.findElementsByXPath("//div[@class = '_1vC4OE _2rQ-NK']");
		for (WebElement eachPrice : price) {
			System.out.println(eachPrice.getText());
		}
		String text2 = driver.findElementByXPath("//div[@class='_3wU53n']").getText();
		driver.findElementByXPath("//div[@class='_3wU53n']").click();
		Set<String> window1 = driver.getWindowHandles();
		List<String> list = new ArrayList<>();
		list.addAll(window1);
		driver.switchTo().window(list.get(1));
		String title = driver.getTitle();
		if(title.contains(text2)) {
			System.out.println("Page Title "+text2+" is populated correctly");
		}
		else { 
			System.out.println("Page title is wrong");
		}
		String text3 = driver.findElementByClassName("_38sUEc").getText();
		System.out.println(text3);
		driver.close();
	}
	
}
