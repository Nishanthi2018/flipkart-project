package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.MainPage;
import wdMethods.ProjectMethods;



public class TC01_Flipkart extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC01_Flipkart";
		testDescription = "Flipkart test cases";
		authors = "Nishanthi";
		category = "Functional";
		dataSheetName = "TC01";
		testNodes = "Flipkart";
	}
	
	@Test
	public void flipKart() throws InterruptedException {		
		new MainPage()
		.closeLogin()
		.clickElectronics()
		.verifyMobileNameTitle()
		.clickNewestFirst()
		.listProducts()
		.listPrice()
		.clickFirstProduct()
		.getRatingsAndReviews();
	}	
}
